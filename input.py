# ==========================================================
# input.py: Contém uma classe responsável por fazer a leitura
# do arquivo de entrada e interpretar as linhas, recuperando
# as informacoes que serão utilizadas na simulacao.
#
# Autores:
#   Álefe Alves Silva
#   Felipe Sampaio Amorim
#   Márcio G V Silva
#   Leonardo Giovanni Prati
#   Michelle Schmitt Gmurczyk
# ==========================================================

class InputAutomato:
    def __init__(self, arquivo_entrada: str) -> None:
        self.arquivo_entrada = arquivo_entrada
        self.dados_entrada = []
        self.num_estados = 0
        self.alfabeto_terminal = []
        self.num_estados_iniciais = 0
        self.estados_iniciais = []
        self.num_estados_de_aceitacao = 0
        self.estados_de_aceitacao = []
        self.num_transicoes = 0
        self.transicoes = []
        self.num_cadeias = 0
        self.cadeias = []

    def le_arquivo(self) -> None:
        with open(self.arquivo_entrada, "r") as f:
            self.dados_entrada = f.readlines()
        self.dados_entrada = [line.strip('\n') for line in self.dados_entrada]
        self.num_estados = int(self.dados_entrada[0])
        self.num_alfabeto_terminal = int(self.dados_entrada[1].split()[0])
        self.alfabeto_terminal = [char for char in self.dados_entrada[1].split()[1:]]
        self.num_estados_iniciais = int(self.dados_entrada[2])
        self.estados_iniciais = [i for i in range(self.num_estados_iniciais)]
        self.num_estados_de_aceitacao = int(self.dados_entrada[3].split()[0]) 
        self.estados_de_aceitacao = [int(indice) for indice in self.dados_entrada[3].split()[1:]]
        self.num_transicoes = int(self.dados_entrada[4])
        for i in range(5, 5 + self.num_transicoes):
            transicao = self.dados_entrada[i].split()
            self.transicoes.append([
                int(transicao[0]), 
                int(transicao[2]),
                self.alfabeto_terminal.index(transicao[1])
            ])
        self.num_cadeias = int(self.dados_entrada[5 + self.num_transicoes])
        for i in range(6 + self.num_transicoes, 6 + self.num_transicoes + self.num_cadeias ):
            self.cadeias.append(self.dados_entrada[i])