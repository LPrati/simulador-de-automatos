# ==========================================================
# afn.py: Contém uma classe responsável pela simulacao de
# AFNs.
#
# Autores:
#   Álefe Alves Silva
#   Felipe Sampaio Amorim
#   Márcio G V Silva
#   Leonardo Giovanni Prati
#   Michelle Schmitt Gmurczyk
# ==========================================================

from typing import List


class AFN:
    def __init__(
        self,
        num_estados: int,
        alfabeto_terminal: List[str],
        estados_iniciais: List[int],
        estados_de_aceitacao: List[int]
    ) -> None:
        self.num_estados = num_estados
        self.alfabeto_terminal = alfabeto_terminal
        self.alfabeto_terminal.append('-')
        self.estados_iniciais = estados_iniciais
        self.estados_de_aceitacao = estados_de_aceitacao
        self.matriz_transicoes = self.inicializa_matriz_transicoes()

    def inicializa_matriz_transicoes(self):
        """Método que inicializa a matriz de transicoes. As linhas e colunas
        são inicializadas individualmente devido à mutabilidade do tipo List
        em Python. Caso fosse inicializado por uma sintaxe como:

            [[[]*num_estados]]*num_estados

        todas as linhas apontariam para um mesmo objeto/endereco de memória.

        Returns:
            List[List[List[]]]: Matriz de transicoes. Número de linhas e de colunas
                                é igual ao número de estados.
        """
        matriz_transicoes = list()
        for i in range(self.num_estados):
            matriz_transicoes.append(list())
            for j in range(self.num_estados):
                matriz_transicoes[i].append(list())
        for i in range(self.num_estados):
            matriz_transicoes[i][i].append(len(self.alfabeto_terminal)-1)
        return matriz_transicoes

    def adiciona_transicao(
        self,
        estado_origem: int,
        estado_destino: int,
        indice_alfabeto_terminal: int
    ) -> None:
        """Método para adicionar transicoes ao automato. Dado um estado de origem,
        um estado destino e um simbolo terminal, o automato possui a transicao
        "origem -> simbolo -> destino" após a invocacao do método.

        Args:
            estado_origem (int): Índice do estado de origem da transicao
            estado_destino (int): Índice do estado destino da transicao
            indice_alfabeto_terminal (int): Índice do símbolo no alfabeto terminal
        """
        self.matriz_transicoes[estado_origem][estado_destino].append(
            indice_alfabeto_terminal
        )

    def verifica_transicao(self, estado_origem: int, estado_destino: int) -> List[str]:
        indices_alfabeto_terminal = self.matriz_transicoes[estado_origem][estado_destino]
        return_list = [self.alfabeto_terminal[indice]
                       for indice in indices_alfabeto_terminal]
        return return_list

    def testa_cadeia_subfuncao(self, estado_inicial: int, cadeia: str) -> bool:
        """Método recursivo para verificacao de validade de uma cadeia

        Args:
            estado_inicial (int): Indice n do estado qn que deve ser utilizado como ponto de partida
            cadeia (str): Cadeia que está sendo testada

        Returns:
            [bool]: True caso a cadeia seja válida, False caso contrario
        """
        if cadeia == '':
            if estado_inicial in self.estados_de_aceitacao:
                return True
            else:
                return False
        else:
            retornos = []
            for destino in range(self.num_estados):
                if cadeia[0] in self.verifica_transicao(estado_inicial, destino):
                    retornos.append(self.testa_cadeia_subfuncao(destino, cadeia[1:]))
            if True in retornos:
                return True
            else:
                return False

    def testa_cadeia(self, cadeia) -> bool:
        """Método que deve ser invocado para verificacao de validade de
        uma cadeia. Invoca o metodo testa_cadeia_subfuncao para todos
        os estados iniciais possíveis do autômato.py

        Args:
            cadeia (str): Cadeia que será testada

        Returns:
            [bool]: True caso a cadeia seja válida, False caso contrario
        """
        retornos = []
        for estado_inicial in self.estados_iniciais:
            retornos.append(self.testa_cadeia_subfuncao(estado_inicial, cadeia))
        if True in retornos:
            return True
        else:
            return False
