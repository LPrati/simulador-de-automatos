# ==========================================================
# main.py: Script principal, que deve ser invocado para
# executar o simulador de autômatos.
#
# Forma de utilizacao: 
#   
#    $python3 main.py <arquivo_entrada> <nome_saida>
#
# <arquivo_entrada> é o caminho para um arquivo de texto
# no formato da especificacao do projeto.
#
# <nome_saida> é o caminho do arquivo que será criado pelo
# programa contendo os resultados no formato da especificacao
# do projeto.
#
# Autores:
#   Álefe Alves Silva
#   Felipe Sampaio Amorim
#   Márcio G V Silva
#   Leonardo Giovanni Prati
#   Michelle Schmitt Gmurczyk
# ==========================================================

import sys

from afd import AFD
from afn import AFN
from input import InputAutomato

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"Número de argumeentos inválidos: 2 esperados, {len(sys.argv)-1} encontrados")
        print("")
        print("Forma de utilizacao do programa (se estiver utilizando um executável .exe):")
        print("SimuladorDeAutomatos.exe <arquivo_de_entrada> <nome_arquivo_saida>")
        print("")
        print("Forma de utilizacao do programa (se estiver utilizando um script .py):")
        print("python3 main.py <arquivo_de_entrada> <nome_arquivo_saida>")
        print("")
        print("Argumentos: ")
        print("<arquivo_de_entrada> é o caminho para um arquivo existente no formato da especificacao do projeto.")
        print("<nome_arquivo_saida> é nome do arquivo que o programa criará com os resultados.")
        exit(0)
    arquivo_entrada = sys.argv[1]
    arquivo_saida = sys.argv[2]

    dados_entrada = InputAutomato(arquivo_entrada)
    dados_entrada.le_arquivo()

    if dados_entrada.num_estados_iniciais == 1:
        automato = AFD(
            dados_entrada.num_estados,
            dados_entrada.alfabeto_terminal,
            dados_entrada.estados_de_aceitacao
        )
    else:
        automato = AFN(
            dados_entrada.num_estados,
            dados_entrada.alfabeto_terminal,
            dados_entrada.estados_iniciais,
            dados_entrada.estados_de_aceitacao
        )

    for transicao in dados_entrada.transicoes:
        automato.adiciona_transicao(
            transicao[0],
            transicao[1],
            transicao[2] 
        )

    resultados = []
    for cadeia in dados_entrada.cadeias:
        if automato.testa_cadeia(cadeia) is True:
            resultados.append("aceita")
        else:
            resultados.append("rejeita")

    with open(arquivo_saida, "w") as f:
        for index, resultado in enumerate(resultados):
            f.write(f"{resultado}")
            if index != len(resultados) - 1:
                f.write("\n")