from afd import AFD
from afn import AFN

def testa_afd():
    # -- Caso de teste definido no documento do projeto

    # Inicializa o objeto e imprime suas propriedades na tela
    afd = AFD(3, ['a', 'b'], [2])
    # print(afd.num_estados)
    # print(afd.alfabeto_terminal)
    # print(afd.estados_de_aceitacao)
    # print(afd.matriz_transicoes)

    # Adiciona as transicoes
    afd.adiciona_transicao(0, 1, 0)
    afd.adiciona_transicao(0, 1, 1)
    afd.adiciona_transicao(1, 1, 0)
    afd.adiciona_transicao(1, 2, 1)
    afd.adiciona_transicao(2, 2, 1)
    afd.adiciona_transicao(2, 0, 0)
    
    # Imprime as transicoes na tela
    # for i in range(afd.num_estados):
    #     for j in range(afd.num_estados):
    #         print(f"Transicao {i} -> {j}: {afd.verifica_transicao(i, j)}")

    # Realiza teste com as cadeias fornecidas
    inputs = [
        ("abbbba", False),
        ("aabbbb", True),
        ("bbabbabbabbb", True),
        ("bbbbbbbbbbb", True),
        ("-", False),
        ("abababababab", False),
        ("bbbbaabbbb", True),
        ("abba", False),
        ("a", False),
        ("aaa", False)
    ]
    for indice, input in enumerate(inputs):
        if afd.testa_cadeia(0, input[0]) == input[1]:
            print(f"Caso teste {indice} passou")
        else:
            print(f"Caso teste {indice} falhou")

def testa_afn():
    afn = AFN(4, ['0', '1'], [0, 1], [2])
    afn.adiciona_transicao(0, 0, 1)
    afn.adiciona_transicao(0, 2, 1)
    afn.adiciona_transicao(1, 1, 0)
    afn.adiciona_transicao(1, 1, 1)
    afn.adiciona_transicao(1, 2, 0)
    afn.adiciona_transicao(2, 0, 0)
    afn.adiciona_transicao(2, 3, 1)

    inputs = [
        ("1110", True),
        ("0101", False),
        ("1011", True),
        ("0011", True),
        ("0111", False),
        ("-", False),
        ("0", True),
        ("1", True),
        ("10", True),
        ("01", False)
    ]
    for indice, input in enumerate(inputs):
        if afn.testa_cadeia_afn(0, input[0]) == input[1]:
            print(f"Caso teste {indice} passou")
        else:
            print(f"Caso teste {indice} falhou")

